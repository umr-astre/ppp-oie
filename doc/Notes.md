---
title: PPP-OIE 
subtitle: Typologies and efficacy of Public-Private Partnerships of veterinary services
author: Facundo Muñoz
date: October 2017
output:
  pdf_document: default
  html_document: default
documentclass: cirad
bibliography: ppp-oie-project.bib
---



## Context

2017-10-30 meeting with Marisa Peyre.

They have survey data from stakeholders from different countries concerning the
implementation of Public-Private Partnerships (PPP) in the animal-disease
surveillance.

The first objective is to classify the different PPP initiatives into
approximately homogeneous groups or typologies, or at least to understand which
dimensions of the multivariate space discriminate best among initiatives.

Later, the efficacy of these initiatives will be examined and put in
relationship whith the previous categorisation.


## Data

- Survey responses on about $n = 57$ PPP initiatives with $p = 18$ questions each.

- Most questions are qualitative (binary, categorical or ordinal).


## Proposed Analyses

- A Multiple-Correspondence Analysis seems appropriate for the construction of
typologies, although it will not infer the groups automatically. It will project
the levels of the answers in a two-dimensional plane, from where some
associations might be revealed, and perhaps the dimenstions themselves can yield
some interpretation.

- We can also use model-based clustering to try to infer the groups. Latent-Class
Models (LCM) are an example [@Hagenaars09ALCA], although they assume conditional-independence, which
might not be appropriate for a survey. Moreover, perhaps clusters are not well
separated and it is better to think of a continuous latent space where PPPs can
live.

- Some ML algorithm such as Random Forests, Neural Networks or Support Vector
Machines are also envisageable if needed [See @Venables02MASS, §12], but these
require knowing the grouping _a priori_ as a _response_ variable, while what we
want is to identify the groups.


- For the analysis of efficiency, some kind of regression, depending on the
nature of the response variable(s).


## Remarks

- Check with Jean-Charles Sicard, who has done this kind of typology analysis
before, although not sure if with qualitative data.

- Check standard practices in analysing survey responses.


# Bibliography