---
title: Material and Methods
author: Facundo Muñoz
date: January 2018
output:
  pdf_document: default
  html_document: default
documentclass: cirad
bibliography: ppp-oie-project.bib
---


## Data Processing

The data about PPPs consist on qualitative assessments on many different aspects of the each partnership, mostly collected from survey responses.
As some of the questions were open-ended, the responses have been systematised and organised into meaningful variables and categories.
The process involved several iterations of variable classification, exploratory analysis and interpretation.

## Multiple Correspondence Analysis (MCA)

MCA [@Venables02MASS, §12; @Husson17Exploratory] is a descriptive tool to explore and summarise a dataset of categorical variables that can be seen as the counterpart of Principal Components Analysis (PCA) for categorical data.
It aims to represent both the PPPs and the variable categories in a Euclidean space $\mathbb{R}^d$ such that the distances between the representations of PPPs are interpretable in terms of _resemblance_ between them.

Thus, PPPs that share the same categorical values for many variables tend to be _close_ in their MCA representation.
On the contrary, PPPs that take mostly different values will typically yield distant MCA representations.
Similarly, categories that are positively associated empirically (i.e., PPPs carrying one of the categories are likely to also carry the other one and vice-versa) also tend to have nearby MCA representations, while negatively associated categories will be represented far apart.

An interesting property of MCA is that both PPPs and categories are represented in the same scale.
Therefore, proximity between PPPs and categories can also be interpreted in the sense that PPPs with some specific characteristic will be represented close to the corresponding category.

Furthermore, the dimensions of the space are sorted in decreasing order of explained variability, working exactly in the same way as PCA for quantitative variables.
This results in an effective way to capture the main trends and patterns in the dataset, while removing some of the _noise_ introduced by the sampling variability.

The method proceeds by first constructing an indicator matrix with PPPs in the rows and all of the categories for every variable in the columns. The element $x_{ik}$ of this table has a value of $1$ if the individual $i$ carries category $k$ and 0 if it does not. From this matrix, sometimes called the _Design Matrix_ or the _Complete Disjunctive Table_ (CDT) depending on the literature and context, dissimilarities among PPPs and categories are computed and embedded into a $d-$dimensional Euclidean space with orthogonal dimensions of decreasing importance.

Some caution is needed when dealing with rare categories.
A category carried only by a small fraction of PPPs can have an exaggerated influence on the analysis.
Part of the problem is accounted for by regrouping certain categories in the iterative process described previously.
This process requires the application of common sense and expert judgement to avoid dissolving meaningful and interesting distinctions between categories.
If some categories remain very rare beyond sensible regrouping, the PPPs therein can be randomly attributed to other categories with probabilities proportional to their sample sizes. This process is known as _Ventilation_.

Another delicate point is the handling of missing values, or lack of response for some variable.
The obvious approach of removing the PPPs with missing values leads to an undesirable loss of information from all the other variables for which responses were collected.
Furthermore, lack of response on some specific variable can be informative on itself.

In this work, we used a MCA representation in dimension $d = 5$ and we _ventilated_ categories with less than 5% of observations (corresponding to 3 PPPs or less).
Missing values for a given variable were replaced by the average values of the implied categories in the indicator matrix.
Specifically, we used the function `MCA()` implemented in the `R`-package `FactoMineR` [@Le08FactoMineR].


## Non-metric Multidimensional Scaling (MDS)

Another approach to reduce dimensionality in a categorical context is known as Non-metric Multidimensional Scaling [@Gordon99Classification].
It introduces alternative measurements of dissimilarities between PPPs, and then proceeds similarly to MCA by computing a set of points in $\mathbb R^d$ whose pairwise distances match as closely as possible the original dissimilarities.

However, this method is only able to represent PPPs in the Euclidean space, not variables or categories.
Thus, many interesting possibilities for interpretation are lost with respect to MCA.
We explored this alternative representation mostly as a confirmatory tool, to make sure that the main conclusions are robust and consistent across different alternative approaches.

As before, we represented the PPPs in a $5-$dimensional Euclidean space from dissimilarities computed with _Gower's_ dissimilarity metric [@Gower71], as implemented in [@Maechler17cluster].
Specifically, the dissimilarity between PPPs $i$ and $j$ is the following weighted average of the contributions of each variable:
$$
	d(i, j) = \frac{\sum_{k=1}^p w_k \delta_{ij}(k) d_k(i,j)}{
	\sum_{k=1}^p w_k \delta_{ij}(k)},
$$
where $w_k$ is the weight of variable $k$, $\delta_{ij}(k)$ is zero when variable $k$ is missing in either or both PPPs ($i$ and $j$) or 1 otherwise and $d_k(i,j)$ is the contributed distance from variable $k$ which is 0 if both values are equal or 1 otherwise.
We used a default weight $w_k = 1$ for all variables, as we did not have any reason to favour any one variable in particular.
We used these dissimilarities to feed _Sammon's_ algorithm for Non-metric MDS [@Venables02MASS].



## K-means Clustering

While the previous methods provide useful visual representations of resemblance among and across PPPs and variables, they don't automatically yield a _classification_ into different typologies.
However, the Euclidean representation (either from MCA or MDS) of PPPs projected into the two Principal axes of variation displayed a triangular pattern that suggested that PPPs were most meaningfully distributed across 3 main groups.

We then used this information to feed the most widely used clustering algorithm: K-means [@Hartigan79Kmeans].
The algorithm finds an _optimal_ classification of the PPPs into the specified number of typologies.
The optimality criterion is based on maximising the variability between groups, thus concentrating the individuals into compact groups separated as much as possible from other groups.

The algorithm proceeds iteratively by computing the group centroids, re-assigning individuals to the closest centroid and evaluating the variability explained by the groups. Thus, it works on an Euclidean space where distances and centroids can be computed and it also requires an initial set of centroids.

We used both the MCA and MDS representations of the PPPs on $\mathbb{R}^5$ and we provided 20 random starting sets of group-centres from the ensemble of individuals to improve the optimisation.
K-means is implemented by the function `kmeans()` in the `R`-package `stats` [@Rstats].


## Agglomerative Hierarchical Clustering

Hierarchical clustering methods [@Rencher02Methods; @Husson17Exploratory] are a popular family of alternative approaches to K-means.
Their main advantage is that the number of groups do not need to be pre-specified in advance.
While in our case the MCA and MDS representations provided sufficient support for the hypothesis of 3 typologies of PPPs, we also explored the results of one of the methods from this family as a confirmatory measure.

These methods require a set of dissimilarities between $n$ individuals.
Therefore, we used the dissimilarities computed previously for the MDS representation.
The algorithm proceeds iteratively by initially assigning each individual to its own cluster and at each stage joining the two most similar clusters, continuing until there is just a single cluster.

The distances between clusters can be computed in a number of ways.
We used Ward's minimum variance method [@Ward63Hierarchical] which merges the clusters that minimise the decrease in the between-group variance.
This criterion aims tends to yield compact, spherical clusters of similar sizes.

The method results in a hierarchical structure of groups of individuals that can be represented graphically in a _dendrogram_, where the height of the forking branches correspond to the distance between the corresponding groups.
Furthermore, it allows _cutting_ the tree at any desired point in order to yield the classification for any number of groups corresponding to the number of branches _cut_ at that level.

This visualisation helps to determine an appropriate number of clusters yielding sufficiently well differentiated groups by looking at the length of the branches, which is proportional to their pairwise distances.
We leveraged the implementation in the R-package `factoextra` [@Rpackage_factoextra].


## Classification Tree

The previous methods aim to establish a classification of the observed PPPs into a set of meaningful typologies.
However, these typologies only become a useful operational tool if it possible to easily classify a new instance of a PPP into the corresponding group.

While this study could be re-made whenever new observations are available, the classification of the current PPPs could potentially change, leading to some PPPs switching groups, or even a complete reclassification into a different number of typologies.
This is of course completely undesirable.

In principle, it is possible to assign a typology to a new PPP in the current framework.
However it involves a series of non-trivial operations like computing the Euclidean coordinates of the new instance in the MCA space, projecting into the first 5 dimensions and finding the closest cluster centre.
While it is feasible to code this procedure, it remains non-operational in practice. 

The objective of this section is to find a set of simple criteria allowing to easily discriminate any new instance of PPP and to allocate it into one of the existing typologies using a few rules that can be followed by hand.

To fulfill this objective we built a _Classification Tree_ [@Hastie17Elements].
It belongs to a family of methods for _Discrimination_ (also known as _Supervised Learning_ in the Machine Learning literature), in contrast to the methods for _Clustering_ (or _Unsupervised Learning_) used in the preceding sections.
The former methods aim to model a _response_ variable (in our case, the typology) given a set of explanatory variables.

Rather than looking for meaningful linear combinations of all available variables that best explain the typology, the Classification Tree splits the set of PPPs into groups using simple binary rules on the values of the current variables trying to match the typologies as best as possible.

Among the many possible methodologies, we used the popular _Classification And Regression Tree_ (CART) algorithm implemented in @Therneau17rpart.
The algorithm produces a tree that recursively splits groups in two using binary rules on one of the variables.
At a given _depth_ the tree yields a number of groups that are assigned to one of the typologies according to the observed typologies in the dataset.

The tree allows to assign a typology to any other PPP by simply _descending_ the tree following the rules and taking the typology of the final _leaf_ of the tree.
We used a tree of depth 2, leading to only two forks and three final _leaves_ that match as best as possible the typologies defined previously.

This tree allows to classify any new PPP by looking at the value of only two variables.
The efficacy of this classification method is analysed next.


## Comparing Predictions

We have used several different methods in order to establish a typology of PPPs and to derive a set of simple rules to classify new instances of PPPs.
Many alternative methods or variations could have been used instead.

In order to make sure that the typology that we defined reflects structures in the data and is not an artifact of the chosen method, we compared the consistency of the classification yielded by 3 alternative methods.

Furthermore, we also developed a _Classification Tree_ as an operational tool to help in the classification of new instances of PPPs.
This procedure involves some degree of simplification, and consequently, of classification error to some extent.
Therefore, we also compared the consistency of the groups yielded by the _Classification Tree_ with respect to the previous methods.

We assessed the consistency of the methodologies by visualising in two different ways the relationship between classifications produced by K-means clustering on both MCA and MDS representations, by Hierarchical Clustering (HC) and by the Classification Tree (CT).

The first visualisation is a _Pairs Plot_ [@Emerson13Generalized] displaying in the diagonal the histograms for the distribution of PPP across typologies for each methodology and _Mosaic Plots_ in the off-diagonal.
In the latter, the area of the tiles is proportional to the number of observations in each combination of typologies for the corresponding pair of methods.
Methods with matching classifications will aggregate most of the PPPs in the diagonal tiles. 
Therefore, this visualisation allows to rapidly and visually quantify their aggregated level of agreement.

However, the previous plot does not allow the identification of the particular PPPs that are classified differently by two given methods.
For this purpose, we used again a MCA representation of PPP's and their classifications by the different methods.
In the corresponding visualisation, the clouds of PPPs that are compact and well separated from the rest are consistently classified in the same typology by all the four methods.
In contrast, PPP's classified differently by at least two methods are represented mid-way from two clusters.


# Bibliography
