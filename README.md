Typologies and efficacy of Public-Private Partnerships of veterinary
services
================
Facundo Muñoz
\- December 2018

This repository contains the data and code the reproduce the results in
the following publication:

**M. Galière, M. Peyre, F. Muñoz, M. Poupaud, A. Dehove, F. Roger, I.
Dieuzy-Labaye (2019)**. Typological analysis of public-private
partnerships in the veterinary domain. *PLoS ONE* 14(10): e0224079
[DOI: 10.1371/journal.pone.0224079](https://doi.org/10.1371/journal.pone.0224079)
